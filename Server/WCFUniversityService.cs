﻿using IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Reflection;
using System.Data;

namespace Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WCFUniversityService" in both code and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class WCFUniversityService : IWCFUniversityService
    {
        private testContext transAction;

        public List<string> getSeminars()
        {
            using (var database = new testContext())
            {
                var seminars = (from s in database.seminar
                                select s.Name).ToList();
                return seminars;
            }
        }

        public List<string> getReferents(string seminar)
        {
            int id = getSeminarId(seminar);
            using (var database = new testContext())
            {
                // if there is no such seminar in database, return all references (for treeView root)
                if (id == 0)
                {
                    return (from r in database.referent
                            select r.NAME).ToList();
                }

                // else return all references wich take part in particular seminar
                var referents = from r in database.referent
                                join q in database.qualref on r.idReferent equals q.Referent_idReferent
                                join s in database.seminar on q.Seminar_idSeminar equals s.idSeminar
                                where s.idSeminar == id
                                select r.NAME;

                return referents.ToList();
            }
        }

        public List<string> getSeminarsForReferent(string referent)
        {
            int id = getReferentId(referent);
            using (var database = new testContext())
            {
                // else return all references wich take part in particular seminar
                var seminars = from r in database.referent
                                join q in database.qualref on r.idReferent equals q.Referent_idReferent
                                join s in database.seminar on q.Seminar_idSeminar equals s.idSeminar
                                where r.idReferent == id
                                select s.Name;

                return seminars.ToList();
            }
        }

        private int getSeminarId(string smnr)
        {
            using (var database = new testContext())
            {
                return (from s in database.seminar
                        where s.Name == smnr
                        select s.idSeminar).SingleOrDefault();
            }
        }

        private int getReferentId(string refernt)
        {
            using (var database = new testContext())
            {
                return (from r in database.referent
                        where r.NAME == refernt
                        select r.idReferent).SingleOrDefault();
            }
        }

        public void addQualRef(string referent, string seminar)
        {
            using (var database = new testContext())
            {
                int refID = getReferentId(referent);
                int semID = getSeminarId(seminar);

                var existQualref = database.qualref.FirstOrDefault(
                    (q) => q.Referent_idReferent == refID && q.Seminar_idSeminar == semID);
                if (existQualref == null)
                {
                    database.qualref.Add(
                        new qualref { Referent_idReferent = refID, Seminar_idSeminar = semID });
                }

                database.SaveChanges();
            }
        }

        // for dynamicly creating dataGridViewColumns
        public List<string> getRefTableTitles()
        {
            Type type = Type.GetType("Server.referent");
            var members = type.GetProperties();
            List<string> titles = new List<string>();
            foreach (PropertyInfo pi in members)
            {
                // disable navigation property
                if (pi.PropertyType == typeof(ICollection<qualref>)) continue;

                titles.Add(pi.Name);
            }
            return titles;

        }

        public string addReferent(DataTable dt, string seminar)
        {
            try
            {
                using (var database = new testContext())
                {
                    int idSeminar = getSeminarId(seminar);
                    foreach (DataRow row in dt.Rows)
                    {
                        referent r = database.referent.Add(new referent
                            {
                                NAME = row["NAME"].ToString(),
                                BIRTHDAY = Convert.ToDateTime(row["BIRTHDAY"]),
                                ADDRESS = row["ADDRESS"].ToString(),
                                CITY = row["CITY"].ToString(),
                                ZIP = row["ZIP"].ToString(),
                                TAX = Convert.ToUInt32(row["TAX"]),
                                MEMO = row["MEMO"].ToString()
                            });
                        if (idSeminar != 0)
                        {
                            database.qualref.Add(new qualref
                                {
                                    Referent_idReferent = r.idReferent,
                                    Seminar_idSeminar = idSeminar
                                });
                        }
                    }
                    database.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "true";
        }

        public void deleteRef(string referent, string seminar)
        {
            int idSeminar = getSeminarId(seminar);
            int idReferent = getReferentId(referent);

            using (var database = new testContext())
            {
                // delete qualref
                if (idSeminar != 0)
                {
                    var qr = database.qualref.SingleOrDefault((q) => q.Referent_idReferent == idReferent && q.Seminar_idSeminar == idSeminar);
                    if (qr != null)
                        database.qualref.Remove(qr);
                }
                // delete referent
                else
                {
                    var r = database.referent.SingleOrDefault((rf) => rf.idReferent == idReferent);
                    if (r != null)
                        database.referent.Remove(r);
                }
                database.SaveChanges();
            }
        }

        public int editRef(DataTable dt)
        {
            int idReferent = Convert.ToInt32(dt.Rows[0][0]);

            transAction = new testContext();
            var refer = transAction.referent.FirstOrDefault((r) => r.idReferent == idReferent);
            if (refer != null)
            {
                refer.NAME = dt.Rows[0]["NAME"].ToString();
                refer.BIRTHDAY = Convert.ToDateTime(dt.Rows[0]["BIRTHDAY"]);
                refer.ADDRESS = dt.Rows[0]["ADDRESS"].ToString();
                refer.CITY = dt.Rows[0]["CITY"].ToString();
                refer.ZIP = dt.Rows[0]["ZIP"].ToString();
                refer.TAX = Convert.ToUInt32(dt.Rows[0]["TAX"]);
                refer.MEMO = dt.Rows[0]["MEMO"].ToString();
            }
            var changed = transAction.ChangeTracker.Entries().Where(x => x.State == EntityState.Modified);

            return changed.Count();
        }

        public void saveEditedChanges()
        {
            transAction.SaveChanges();
            transAction.Dispose();
        }

        public List<object> getRefInfo(string referent)
        {
            int idReferent = getReferentId(referent);
            using (var database = new testContext())
            {
                var refer = database.referent.SingleOrDefault((r) => r.idReferent == idReferent);
                if (refer != null)
                {
                    List<object> l = new List<object>();
                    l.Add(refer.idReferent);
                    l.Add(refer.NAME);
                    l.Add(refer.BIRTHDAY.Value.ToShortDateString());
                    l.Add(refer.ADDRESS);
                    l.Add(refer.ZIP);
                    l.Add(refer.CITY);
                    l.Add(refer.TAX);
                    l.Add(refer.MEMO);

                    return l;
                }

            }
            return null;
        }

        public void clearQualRef(string referent)
        {
            int idReferent = getReferentId(referent);
            using(var database = new testContext())
            {
                var qualrefs = (from q in database.qualref
                                where q.Referent_idReferent == idReferent
                                select q).ToList();

                foreach (var qualref in qualrefs)
                {
                    database.qualref.Remove(qualref);
                }
                database.SaveChanges();
            }
        }
    }
}

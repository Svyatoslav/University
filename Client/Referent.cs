﻿using IService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class Referent : Form
    {
        IWCFUniversityService proxy;
        string seminar;
        string referent;
        List<string> seminarsList = new List<string>();

        public Referent(IWCFUniversityService _proxy, string _seminar, string _referent = "")
        {
            InitializeComponent();
            proxy = _proxy;
            seminar = _seminar;
            referent = _referent;

            setDataSource();


        }

        private void setDataSource()
        {
            // set dataGridView datasource
            var titlesList = proxy.getRefTableTitles();
            DataTable dt = new DataTable();
            dt.TableName = "Referents";
            foreach (string title in titlesList)
            {
                DataColumn dgc = new DataColumn();
                dgc.ColumnName = title;
                dt.Columns.Add(dgc);
            }
            // readonly id. (autoincrement = true)
            dt.Columns[0].ReadOnly = true;


            if (referent != "")
            {
                DataRow dr = dt.NewRow();
                List<object> l = proxy.getRefInfo(referent);
                for (int i = 0; i < l.Count; i++)
                {
                    dr[i] = l[i];
                }
                dt.Rows.Add(dr);
                foreach (string seminar in proxy.getSeminars())
                {
                    lvAllSeminars.Items.Add(new ListViewItem(seminar));
                }
                lvActiveSeminars.Items.Clear();
                foreach (string seminar in proxy.getSeminarsForReferent(referent))
                {
                    lvActiveSeminars.Items.Add(new ListViewItem(seminar));
                }

                foreach (ListViewItem item in lvActiveSeminars.Items)
                {
                    seminarsList.Add(item.Text);
                }
                
            }

            dgvReferent.DataSource = dt;
            this.Width = 73 + titlesList.Count * 100;
        }

        private void refreshActiveSeminars()
        {
            
        }

        private void Referent_FormClosing(object sender, FormClosingEventArgs e)
        {
            // referent = "" means user trying to add new referent
            if (referent == "")
            {
                DataTable dt = (dgvReferent.DataSource as DataTable);
                string result = proxy.addReferent(dt, seminar);
                if (result != "true")
                {
                    MessageBox.Show(result, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw new ArgumentException();
                }
            }

            // otherwise user trying to modify referent
            else
            {
                
                DataTable dt = (dgvReferent.DataSource as DataTable);
                int result = proxy.editRef(dt);

                // ckecking. seminars were edited?
                bool dataEdited = false;
                List<string> newSeminars = new List<string>();
                foreach (ListViewItem item in lvActiveSeminars.Items)
                {
                    newSeminars.Add(item.Text);
                }

                // need to know what list is bigger to loop throught all elements
                int count = (seminarsList.Count > newSeminars.Count) ? seminarsList.Count : newSeminars.Count;
                try
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (seminarsList[i] != newSeminars[i]) dataEdited = true;
                    }
                }
                catch(ArgumentOutOfRangeException)
                {
                    dataEdited = true;
                }
                
                // if data was edited, ask for apply changes ?
                if (result > 0 || dataEdited)
                {
                    DialogResult dr = MessageBox.Show("Apply changes?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == System.Windows.Forms.DialogResult.Yes)
                    {
                        proxy.clearQualRef(referent);
                        foreach (ListViewItem item in lvActiveSeminars.Items)
                        {
                            proxy.addQualRef(referent, item.Text);
                        }
                        proxy.saveEditedChanges();
                    }
                }
            }
        }

        private void btnAddSeminar_Click(object sender, EventArgs e)
        {
            if(lvAllSeminars.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please choose seminar first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            foreach (ListViewItem seminar in lvAllSeminars.SelectedItems)
            {
                // checking, if current item is allready added.
                bool haveItem = false;
                foreach (ListViewItem item in lvActiveSeminars.Items)
                {
                    if (item.Text == seminar.Text) haveItem = true;
                }
                if (haveItem)
                {
                    haveItem = false;
                    continue;
                }
                // if no, add this item
                lvActiveSeminars.Items.Add(new ListViewItem(seminar.Text));
            }
        }

        private void btnDeleteSeminar_Click(object sender, EventArgs e)
        {
            if (lvActiveSeminars.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please choose seminar first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ListView.SelectedListViewItemCollection l = lvActiveSeminars.SelectedItems;
            foreach (ListViewItem item in l)
            {
                lvActiveSeminars.Items.Remove(item);
            }

        }
    }
}

﻿namespace Client
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmTop = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tvSeminars = new System.Windows.Forms.TreeView();
            this.wbPreviewPane = new System.Windows.Forms.WebBrowser();
            this.lvReferents = new System.Windows.Forms.ListView();
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbDelete = new System.Windows.Forms.PictureBox();
            this.pbEdit = new System.Windows.Forms.PictureBox();
            this.pbAdd = new System.Windows.Forms.PictureBox();
            this.tmTop.SuspendLayout();
            this.lvContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAdd)).BeginInit();
            this.SuspendLayout();
            // 
            // tmTop
            // 
            this.tmTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.tmTop.Location = new System.Drawing.Point(0, 0);
            this.tmTop.Name = "tmTop";
            this.tmTop.Size = new System.Drawing.Size(662, 24);
            this.tmTop.TabIndex = 0;
            this.tmTop.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // tvSeminars
            // 
            this.tvSeminars.AllowDrop = true;
            this.tvSeminars.Location = new System.Drawing.Point(12, 63);
            this.tvSeminars.Name = "tvSeminars";
            this.tvSeminars.Size = new System.Drawing.Size(192, 370);
            this.tvSeminars.TabIndex = 1;
            this.tvSeminars.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvSeminars_NodeMouseClick);
            this.tvSeminars.DragDrop += new System.Windows.Forms.DragEventHandler(this.tvSeminars_DragDrop);
            this.tvSeminars.DragEnter += new System.Windows.Forms.DragEventHandler(this.tvSeminars_DragEnter);
            // 
            // wbPreviewPane
            // 
            this.wbPreviewPane.Location = new System.Drawing.Point(210, 264);
            this.wbPreviewPane.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbPreviewPane.Name = "wbPreviewPane";
            this.wbPreviewPane.Size = new System.Drawing.Size(440, 169);
            this.wbPreviewPane.TabIndex = 3;
            // 
            // lvReferents
            // 
            this.lvReferents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName});
            this.lvReferents.ContextMenuStrip = this.lvContextMenu;
            this.lvReferents.FullRowSelect = true;
            this.lvReferents.Location = new System.Drawing.Point(210, 63);
            this.lvReferents.Name = "lvReferents";
            this.lvReferents.Size = new System.Drawing.Size(440, 195);
            this.lvReferents.TabIndex = 4;
            this.lvReferents.UseCompatibleStateImageBehavior = false;
            this.lvReferents.View = System.Windows.Forms.View.Details;
            this.lvReferents.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvReferents_MouseClick);
            this.lvReferents.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lvReferents_MouseDown);
            // 
            // colName
            // 
            this.colName.Text = "Name";
            this.colName.Width = 93;
            // 
            // lvContextMenu
            // 
            this.lvContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.editToolStripMenuItem});
            this.lvContextMenu.Name = "lvContextMenu";
            this.lvContextMenu.Size = new System.Drawing.Size(108, 70);
            this.lvContextMenu.Opened += new System.EventHandler(this.lvContextMenu_Opened);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // pbDelete
            // 
            this.pbDelete.Image = global::Client.Properties.Resources.delete;
            this.pbDelete.Location = new System.Drawing.Point(114, 27);
            this.pbDelete.Name = "pbDelete";
            this.pbDelete.Size = new System.Drawing.Size(35, 30);
            this.pbDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDelete.TabIndex = 7;
            this.pbDelete.TabStop = false;
            this.pbDelete.Click += new System.EventHandler(this.pbDelete_Click);
            this.pbDelete.MouseEnter += new System.EventHandler(this.pbAdd_MouseEnter);
            this.pbDelete.MouseLeave += new System.EventHandler(this.pbAdd_MouseLeave);
            // 
            // pbEdit
            // 
            this.pbEdit.Image = global::Client.Properties.Resources.edit;
            this.pbEdit.Location = new System.Drawing.Point(64, 27);
            this.pbEdit.Name = "pbEdit";
            this.pbEdit.Size = new System.Drawing.Size(35, 30);
            this.pbEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbEdit.TabIndex = 6;
            this.pbEdit.TabStop = false;
            this.pbEdit.Click += new System.EventHandler(this.pbEdit_Click);
            this.pbEdit.MouseEnter += new System.EventHandler(this.pbAdd_MouseEnter);
            this.pbEdit.MouseLeave += new System.EventHandler(this.pbAdd_MouseLeave);
            // 
            // pbAdd
            // 
            this.pbAdd.Image = global::Client.Properties.Resources.add;
            this.pbAdd.Location = new System.Drawing.Point(12, 27);
            this.pbAdd.Name = "pbAdd";
            this.pbAdd.Size = new System.Drawing.Size(35, 30);
            this.pbAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAdd.TabIndex = 5;
            this.pbAdd.TabStop = false;
            this.pbAdd.Click += new System.EventHandler(this.pbAdd_Click);
            this.pbAdd.MouseEnter += new System.EventHandler(this.pbAdd_MouseEnter);
            this.pbAdd.MouseLeave += new System.EventHandler(this.pbAdd_MouseLeave);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 445);
            this.Controls.Add(this.pbDelete);
            this.Controls.Add(this.pbEdit);
            this.Controls.Add(this.pbAdd);
            this.Controls.Add(this.lvReferents);
            this.Controls.Add(this.wbPreviewPane);
            this.Controls.Add(this.tvSeminars);
            this.Controls.Add(this.tmTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.tmTop;
            this.Name = "MainForm";
            this.Text = "University";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tmTop.ResumeLayout(false);
            this.tmTop.PerformLayout();
            this.lvContextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAdd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip tmTop;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.TreeView tvSeminars;
        private System.Windows.Forms.WebBrowser wbPreviewPane;
        private System.Windows.Forms.ListView lvReferents;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.PictureBox pbAdd;
        private System.Windows.Forms.PictureBox pbEdit;
        private System.Windows.Forms.PictureBox pbDelete;
        private System.Windows.Forms.ContextMenuStrip lvContextMenu;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
    }
}


﻿namespace Client
{
    partial class Referent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcReferentInfo = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvReferent = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lvAllSeminars = new System.Windows.Forms.ListView();
            this.lvActiveSeminars = new System.Windows.Forms.ListView();
            this.colSeminars = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colActiveSeminars = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnAddSeminar = new System.Windows.Forms.Button();
            this.btnDeleteSeminar = new System.Windows.Forms.Button();
            this.tcReferentInfo.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReferent)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcReferentInfo
            // 
            this.tcReferentInfo.Controls.Add(this.tabPage1);
            this.tcReferentInfo.Controls.Add(this.tabPage2);
            this.tcReferentInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcReferentInfo.Location = new System.Drawing.Point(0, 0);
            this.tcReferentInfo.Name = "tcReferentInfo";
            this.tcReferentInfo.SelectedIndex = 0;
            this.tcReferentInfo.Size = new System.Drawing.Size(857, 368);
            this.tcReferentInfo.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvReferent);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(849, 342);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Referent";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvReferent
            // 
            this.dgvReferent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReferent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReferent.Location = new System.Drawing.Point(3, 3);
            this.dgvReferent.Name = "dgvReferent";
            this.dgvReferent.Size = new System.Drawing.Size(843, 336);
            this.dgvReferent.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnDeleteSeminar);
            this.tabPage2.Controls.Add(this.btnAddSeminar);
            this.tabPage2.Controls.Add(this.lvActiveSeminars);
            this.tabPage2.Controls.Add(this.lvAllSeminars);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(849, 342);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Seminar";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lvAllSeminars
            // 
            this.lvAllSeminars.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colSeminars});
            this.lvAllSeminars.FullRowSelect = true;
            this.lvAllSeminars.Location = new System.Drawing.Point(31, 6);
            this.lvAllSeminars.Name = "lvAllSeminars";
            this.lvAllSeminars.Size = new System.Drawing.Size(327, 328);
            this.lvAllSeminars.TabIndex = 0;
            this.lvAllSeminars.UseCompatibleStateImageBehavior = false;
            this.lvAllSeminars.View = System.Windows.Forms.View.Details;
            // 
            // lvActiveSeminars
            // 
            this.lvActiveSeminars.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colActiveSeminars});
            this.lvActiveSeminars.Location = new System.Drawing.Point(492, 3);
            this.lvActiveSeminars.Name = "lvActiveSeminars";
            this.lvActiveSeminars.Size = new System.Drawing.Size(327, 328);
            this.lvActiveSeminars.TabIndex = 1;
            this.lvActiveSeminars.UseCompatibleStateImageBehavior = false;
            this.lvActiveSeminars.View = System.Windows.Forms.View.Details;
            // 
            // colSeminars
            // 
            this.colSeminars.Text = "All seminars";
            this.colSeminars.Width = 90;
            // 
            // colActiveSeminars
            // 
            this.colActiveSeminars.Text = "Active seminars";
            this.colActiveSeminars.Width = 94;
            // 
            // btnAddSeminar
            // 
            this.btnAddSeminar.Location = new System.Drawing.Point(387, 119);
            this.btnAddSeminar.Name = "btnAddSeminar";
            this.btnAddSeminar.Size = new System.Drawing.Size(75, 23);
            this.btnAddSeminar.TabIndex = 2;
            this.btnAddSeminar.Text = ">>";
            this.btnAddSeminar.UseVisualStyleBackColor = true;
            this.btnAddSeminar.Click += new System.EventHandler(this.btnAddSeminar_Click);
            // 
            // btnDeleteSeminar
            // 
            this.btnDeleteSeminar.Location = new System.Drawing.Point(387, 160);
            this.btnDeleteSeminar.Name = "btnDeleteSeminar";
            this.btnDeleteSeminar.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteSeminar.TabIndex = 3;
            this.btnDeleteSeminar.Text = "<<";
            this.btnDeleteSeminar.UseVisualStyleBackColor = true;
            this.btnDeleteSeminar.Click += new System.EventHandler(this.btnDeleteSeminar_Click);
            // 
            // Referent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(857, 368);
            this.Controls.Add(this.tcReferentInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Referent";
            this.Text = "Referent";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Referent_FormClosing);
            this.tcReferentInfo.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReferent)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcReferentInfo;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvReferent;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView lvActiveSeminars;
        private System.Windows.Forms.ListView lvAllSeminars;
        private System.Windows.Forms.ColumnHeader colActiveSeminars;
        private System.Windows.Forms.ColumnHeader colSeminars;
        private System.Windows.Forms.Button btnDeleteSeminar;
        private System.Windows.Forms.Button btnAddSeminar;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IService;
using System.ServiceModel;

namespace Client
{
    public partial class MainForm : Form
    {
        ChannelFactory<IWCFUniversityService> channelFactory = new
            ChannelFactory<IWCFUniversityService>("UniversityServiceEndpoint");
        IWCFUniversityService proxy;

        public MainForm()
        {
            InitializeComponent();
            proxy = channelFactory.CreateChannel();
            pbAdd.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            tvSeminars.Nodes.Add("Seminars");
            foreach (string seminar in proxy.getSeminars())
            {
                tvSeminars.Nodes[0].Nodes.Add(seminar);
            }
        }

        // show references for selected node
        private void tvSeminars_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            reloadSeminars(e.Node.Text);
        }

        private void reloadSeminars(string node)
        {
            lvReferents.Items.Clear();
            List<string> referents = proxy.getReferents(node);
            {
                foreach (string referent in referents)
                {
                    lvReferents.Items.Add(new ListViewItem(referent));
                }
            }
        }

        // allow drug, set data to drug
        private void lvReferents_MouseDown(object sender, MouseEventArgs e)
        {
            if (lvReferents.SelectedItems.Count == 0) return;
            List<string> referents = new List<string>();
            foreach (ListViewItem name in lvReferents.SelectedItems)
            {
                referents.Add(name.Text);
            }

            lvReferents.DoDragDrop(referents, DragDropEffects.Copy);
        }

        private void tvSeminars_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        private void tvSeminars_DragDrop(object sender, DragEventArgs e)
        {
            // get node to drop in
            TreeNode seminar = this.tvSeminars.GetNodeAt(this.tvSeminars.PointToClient(new Point(e.X, e.Y)));
            if (seminar == null) { return; }

            // get data to drop
            object data = e.Data.GetData(typeof(List<string>));
            if (data == null) { return; }


            foreach (string name in data as List<string>)
            {
                proxy.addQualRef(name, seminar.Text);
                proxy.saveEditedChanges();
            }
        }

        private void pbAdd_MouseEnter(object sender, EventArgs e)
        {
            (sender as PictureBox).Width += 5;
            (sender as PictureBox).Height += 5;
        }

        private void pbAdd_MouseLeave(object sender, EventArgs e)
        {
            (sender as PictureBox).Width -= 5;
            (sender as PictureBox).Height -= 5;
        }

        private void pbAdd_Click(object sender, EventArgs e)
        {
            string s = "";
            if (tvSeminars.SelectedNode != null) s = tvSeminars.SelectedNode.Text;
            new Referent(proxy, s).Show();
        }

        private void pbDelete_Click(object sender, EventArgs e)
        {
            string referent = lvReferents.SelectedItems[0].Text;
            string seminar = tvSeminars.SelectedNode.Text;

            proxy.deleteRef(referent, seminar);
            proxy.saveEditedChanges();
            reloadSeminars(tvSeminars.SelectedNode.Text);
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pbAdd_Click(pbAdd, null);
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pbDelete_Click(pbDelete, null);
        }

        private void pbEdit_Click(object sender, EventArgs e)
        {
            if (lvReferents.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select referent first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string referent = lvReferents.SelectedItems[0].Text;

            new Referent(proxy, "", referent).Show();
        }

        private void lvContextMenu_Opened(object sender, EventArgs e)
        {
            if (lvReferents.SelectedItems.Count == 0)
            {
                editToolStripMenuItem.Enabled = false;
                deleteToolStripMenuItem.Enabled = false;
            }
            else
            {
                editToolStripMenuItem.Enabled = true;
                deleteToolStripMenuItem.Enabled = true;
            }
        }

        private void lvReferents_MouseClick(object sender, MouseEventArgs e)
        {
            if (lvReferents.SelectedItems.Count <= 0) return;
            string aspAdr = Properties.Settings.Default.aspUrl;
            string referent = lvReferents.SelectedItems[0].Text;

            wbPreviewPane.Navigate(aspAdr + referent);
        }
    }
};
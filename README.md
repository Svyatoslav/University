Тестове завдання


Для запуску сервісу на свому iis сервері необхідно змінити service end point для всіх проектів в app.config та web.config файлах.

В client\app.config властивості <setting name="aspUrl" serializeAs="String"> потрібно присвоїти замість localhost:49529 імя власного домена.

В server\app.config потрібно налаштувати строку підключення до бази даних

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IService;
using System.ServiceModel;

namespace PreviewPaneProject
{
    public partial class PreviewPane : System.Web.UI.Page
    {
        ChannelFactory<IWCFUniversityService> channelFactory = new
            ChannelFactory<IWCFUniversityService>("UniversityServiceEndpoint");
        IWCFUniversityService proxy;


        protected void Page_Load(object sender, EventArgs e)
        {
            proxy = channelFactory.CreateChannel();

            string referent = Request.QueryString["ref"].ToString();

            List<object> refInfo = proxy.getRefInfo(referent);
            List<string> tableTitles = proxy.getRefTableTitles();

            tPreview.Rows.Add(new TableRow());
            tPreview.Rows.Add(new TableRow());
            foreach (string title in tableTitles)
            {
                TableCell tc = new TableCell();
                tc.Text = title;
                tPreview.Rows[0].Cells.Add(tc);
            }

            foreach (object item in refInfo)
            {
                TableCell tc = new TableCell();
                tc.Text = item.ToString();
                tPreview.Rows[1].Cells.Add(tc);
            }
        }
    }
}
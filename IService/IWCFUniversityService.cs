﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;

namespace IService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWCFUniversityService" in both code and config file together.
    [ServiceContract]
    public interface IWCFUniversityService
    {
        [OperationContract]
        List<string> getSeminars();

        [OperationContract]
        List<string> getReferents(string seminar);

        [OperationContract]
        List<string> getSeminarsForReferent(string referent);

        [OperationContract]
        void addQualRef(string referent, string seminar);

        [OperationContract]
        List<string> getRefTableTitles();

        [OperationContract]
        string addReferent(DataTable dt, string seminar);

        [OperationContract]
        void deleteRef(string referent, string seminar);

        [OperationContract]
        int editRef(DataTable dt);

        [OperationContract]
        List<object> getRefInfo(string referent);

        [OperationContract]
        void saveEditedChanges();

        [OperationContract]
        void clearQualRef(string referent);
    }
}
